const colors = {
  // Core
  background: '#ffffff', // background
  primary: '#52CBC2', //  darker
  secondary: '#90DBD7', // lighter
  oposite: '#FF5F6A', //
  error: '#ff0000',
  // Text
  textLight: '#3E4A59',
  textMedium: '#425256',
  textDark: '#4C495B',
  // Grey shades
  greyLight: '#F7F7F7',
  greyMedium: '#ECF1F8',
  greyDark: '#3E4A59',
  // black/white
  black: '#000000',
  white: '#ffffff',
};

export default colors;
